const getSum = (str1, str2) => {
   const isString = (str) => {
      return typeof str === "string";
   };

   const notOnlyNumbers = (str) => {
      return /[^\d]/g.test(str);
   };

   if (
      !isString(str1) ||
      !isString(str2) ||
      notOnlyNumbers(str1) ||
      notOnlyNumbers(str2)
   ) {
      return false;
   }

   return String(BigInt(str1) + BigInt(str2));
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
   let countAuthorPosts = 0,
      countAuthorComments = 0;
   for (let post of listOfPosts) {
      if (post["author"] === authorName) {
         countAuthorPosts++;
      }

      if (post.hasOwnProperty("comments")) {
         for (let comment of post["comments"]) {
            if (comment["author"] === authorName) {
               countAuthorComments++;
            }
         }
      }
   }

   return `Post:${countAuthorPosts},comments:${countAuthorComments}`;
};

const tickets = (people) => {
   const counts = [0, 0, 0];

   for (let value of people) {
      switch (value) {
         case 25: {
            counts[0]++;
            break;
         }
         case 50: {
            counts[1]++;
            counts[0]--;
            break;
         }
         default: {
            if (counts[1] > 0) {
               counts[1]--;
               counts[0]--;
            } else {
               counts[0] -= 3;
            }

            counts[2]++;
         }
      }

      if (counts[0] < 0 || counts[1] < 0 || counts[2] < 0) {
         return "NO";
      }
   }

   return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
